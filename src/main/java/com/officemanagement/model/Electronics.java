package com.officemanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

//@Indexed
@Entity

@Table(name="Electronics")
public class Electronics {
	@Column(name="ID")
private String id;
	@Column(name="NAME")
private String name;
	@Column(name="DESCRIPTION")
private String description;
	
public Electronics() {
		super();
		// TODO Auto-generated constructor stub
	}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public Electronics(String id, String name, String description) {
	super();
	this.id = id;
	this.name = name;
	this.description = description;
}

}
