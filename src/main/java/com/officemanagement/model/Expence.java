package com.officemanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;



//@Indexed
@Entity

@Table(name="EXPENCE")
public class Expence {
	@Column(name="ID")
private String id;
	@Column(name="NAME")
private String name;
	@Column(name="DATE")
private String date;
	@Column(name="DETAILS")
private String details;
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getDetails() {
	return details;
}
public void setDetails(String details) {
	this.details = details;
}
public Expence(String id, String name, String date, String details) {
	super();
	this.id = id;
	this.name = name;
	this.date = date;
	this.details = details;
}


}
