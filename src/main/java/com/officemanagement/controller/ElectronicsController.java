package com.officemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.officemanagement.model.Electronics;
import com.officemanagement.service.ElectronicsService;


@Controller
@RequestMapping(value="/electronics")
public class ElectronicsController {

	@Autowired
	ElectronicsService electronicsservice;
		
	@RequestMapping(value="/addelectronics", method=RequestMethod.GET)
	public ModelAndView addelectronics() {
		
		ModelAndView model= new ModelAndView();
		
	Electronics electronics= new Electronics();
		model.addObject("electronicsForm",electronics);
		model.setViewName("electronics_form");
		
		return model;
		
	}
}
