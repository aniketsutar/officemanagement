package com.officemanagement.Repository;

import org.springframework.data.repository.CrudRepository;

import com.officemanagement.model.Electronics;

public interface ElectronicsRepository extends CrudRepository < Electronics , Long > {

}
