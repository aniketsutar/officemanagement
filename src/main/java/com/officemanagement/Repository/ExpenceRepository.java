package com.officemanagement.Repository;

import org.springframework.data.repository.CrudRepository;

import com.officemanagement.model.Expence;

public interface ExpenceRepository extends CrudRepository < Expence , Long > {

}
